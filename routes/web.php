<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
// どちらも閲覧可能
Route::get('/', 'HomeController@allposts')->name('allposts');

// guestのみアクセス可
Route::middleware('guest')->group(function () {
});
// ユーザーのみアクセス可
Route::middleware('auth')->group(function () {
  Route::get('/users', 'HomeController@index')->name('users');
  Route::get('/posts', 'PostsController@index')->name('posts.index');
  Route::get("/posts/create","PostsController@create")->name('posts.create');
  Route::post('/posts', 'PostsController@store')->name('posts.store');
  
  Route::get('/users/edit', 'UsersController@edit')->name('users.edit');
  Route::post('/users/update/{user}', 'UsersController@update');
});