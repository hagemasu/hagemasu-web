@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12 text-center">
            <h1> みんなのHAGEMASU</h1> 
        </div>
        @if (count($posts) > 0)
        @foreach ($posts as $post)
            <div class="card col-md-5 m-2 pt-4">
                <img class="card-img-top img-fluid hagemasu-post"
                     src="{{ $post->image_filepath }}"/>
                <div class="card-body">
                <h5 class="card-title">UserName: {{ $post->user->name }}</h5>
                    <p class="card-text">{{ $post->content }}</p>
                    <p class="card-text"><small class="text-muted">Last updated {{$post->updated_at}}</small></p>
                </div>
            </div>
        @endforeach
        @else
            <picture>
                <img src="{{ asset('/img/no_hage_image.png') }}">
            </picture>
        @endif
    </div>

        {{ $posts->links('partials.pagination') }}
@endsection
