@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="card-header">Dashboard</div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-4 col-xs-12 col-sm-6 col-lg-4">
                        <img src="{{ Gravatar::src(auth()->user()->email, 200) }}" alt="gravatar img" class="img">
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-6 col-lg-8">
                        <div style="border-bottom:1px solid black;">
                            <h2>{{ auth()->user()->name }}</h2>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mt-2">
                                <a href="{{ route('posts.create') }}"
                                   class="btn btn-block btn-outline-primary">
                                    Post Your HAGEMASU!!
                                    
                                </a>
                            </div>
                            <div class="col-md-12 mt-2">
                                <a href="{{ route('posts.index') }}"
                                   class="btn btn-block btn-outline-secondary">
                                    Look Back Your HAGEMASU.
                                </a>
                            </div>
                            <div class="col-md-12 mt-2">
                                <a href="{{ route('users.edit') }}"
                                   class="btn btn-block btn-outline-primary">
                                    Edit Your Profile.
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
