@if ($paginator->hasPages())
    <!-- mdのみ表示 -->
    <div class="mt-2 d-block d-md-none">
        <nav aria-label="@lang('pagination.previous')">
            <ul class="pagination justify-content-center" role="navigation">
                <li>
                    @if ($paginator->onFirstPage())
                        <a href="#" class="btn page-link badge-pill text-muted disabled" aria-disabled="true"><span aria-hidden="true">&lsaquo;Prev</span></a>
                    @else
                        <a href="{{ $paginator->previousPageUrl() }}" class="btn page-link badge-pill">&lsaquo;Prev</a>
                    @endif
                </li>
                <li class="mx-2 btn badge-pill">
                    {{ $paginator->currentPage() }}
                </li>
                <li>
                    @if ($paginator->hasMorePages())
                        <a class="btn page-link badge-pill" href="{{ $paginator->nextPageUrl() }}">Next &rsaquo;</a>
                    @else
                        <a href="#" class="btn page-link badge-pill text-muted disabled" aria-hidden="true"><span aria-hidden="true">Next &rsaquo;</span></a>
                    @endif
                </li>
            </ul>
        </nav>
    </div>

    <!-- mdのみ非表示 -->
    <div class="mt-2 d-none d-md-block">
        <ul class="pagination justify-content-center" role="navigation">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span class="page-link" aria-hidden="true">&lsaquo;</span>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}"
                       rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>
                        @else
                            <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span class="page-link" aria-hidden="true">&rsaquo;</span>
                </li>
            @endif
        </ul>
        </div>
@endif
