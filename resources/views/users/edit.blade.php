@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="card-header">プロフィール編集</div>

            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6 col-xs-12 col-sm-6 col-lg-4">
                        <img src="{{ Gravatar::src(auth()->user()->email, 200) }}" alt="gravatar img" class="img">
                    </div>
                    <div class="col-md-12 col-lg-8">
                        <form action="{{ url('users/update/' . $user->id) }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="InputName">名前</label>
                                <input type="text" name="name" id="InputName" class="form-control" value="{{ $user->name }}">
                            </div>
                            <div class="form-group">
                                <label for="InputEmail">メールアドレス</label>
                                <input type="email" name="email" id="InputEmail" class="form-control" value="{{ $user->email }}">
                            </div>
                            <div class="form-group">
                                <label>HAGEMASU公開設定</label>
                            </div>
                            <div class="form-group">
                                <label class="form-check-inline">
                                    <input type="radio" name="public_flag" value="1" @if(old('public_flag')=="1") checked @endif> 公開する
                                </label>
                                <label class="form-check-inline">
                                    <input type="radio" name="public_flag" value="0" @if(old('public_flag')!=="1") checked @endif> 公開しない
                                </label>
                            </div>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary pull-right">変更する</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
