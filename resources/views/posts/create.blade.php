@extends('layouts.app')

@section('content')
<div class="container">
  <div class="col-8">
    <h1>HAGEMASUを投稿する</h1>
    <form enctype="multipart/form-data" action="/posts" method="POST">
        @csrf
        <div class="form-group">
          <label for="InputContent">コメント</label>
          <textarea class="form-control-file" id="InputContent" name="content"></textarea>
        </div>
        <div class="form-group">
          <label for="InputImage">画像アップロード</label>
          <input type="file" class="form-control-file" id="InputImage" name="file" required>
        </div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
        @endif
        
        
        
        
        
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
</div>
@endsection



