<?php /** @var \App\Post $post */ ?>

@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex align-items-center">
                        <h2 class="m-2">Your HAGEMASU History</h2>
                        <div class="ml-auto">
                            <a href="{{ route('posts.create') }}" class="btn btn-outline-primary">
                                New Post
                            </a>
                        </div>
                    </div>
                </div>
                @if (count($posts) > 0)
                    <ul class="list-group list-group-flush">
                        @foreach ($posts as $post)
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="d-flex">
                                            <img class="card-img-top img-fluid hagemasu-post"
                                                 src="{{ $post->image_filepath }}"/>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="d-flex flex-column hagemasu-post">
                                            <div class="flex-grow-1">
                                                {{ $post->content }}
                                            </div>
                                            <div class="text-right">
                                                <span class="text-muted">Posted on {{ $post->updated_at }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    {{ $posts->links('partials.pagination') }}
                @else
                    <div class="card-body">
                        <h3>No Data</h3>
                        <p>
                            You should post your HAGEMASU right now!
                        </p>
                    </div>
                @endif
            </div>

        </div>
    </div>

@endsection