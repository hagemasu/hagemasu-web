<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * @var string ファイルアップロード先のディレクトリ
     */
    private $image_path;

    /**
     * PostsTableSeeder constructor.
     */
    public function __construct()
    {
        $this->image_path = storage_path('app/public/images');
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // テーブルの中身を消して、物理ファイルも消す
        DB::table('posts')->delete();
        foreach (glob("{$this->image_path}/*.jpg") as $image_file) {
            unlink($image_file);
        }

        // 先に users テーブルを用意する
        $this->call(UsersTableSeeder::class);

        // 画像を保存するフォルダがなければ作成する
        if (!File::exists($this->image_path)) {
            File::makeDirectory($this->image_path);
        }

        $faker = Faker\Factory::create();
        $users = User::all();
        // ユーザに応じて用意する投稿数を変化させる
        $post_counts = [
            'alice' => 90,
            'bob' => 70,
            'carol' => 50,
            'dave' => 30,
            'eve' => 10,
        ];
        foreach ($users as $user) {
            if (array_key_exists($user->name, $post_counts)) {
                $post_date = Carbon::now()->addDay(-100);
                $image_file = $faker->image($this->image_path, 320, 240, null, false);

                // $post_counts の値にしたがって投稿を作成する
                for ($i = 0 ; $i < $post_counts[$user->name]; $i++) {
                    $post_date->addDay(1);
                    DB::table('posts')->insert([
                        'image_filepath' => $image_file,
                        'content' => $faker->text(200),
                        'user_id' => $user->id,
                        'created_at' => $post_date,
                        'updated_at' => $post_date,
                    ]);
                }
            } else {
                // $post_counts にないユーザは適当に１０件作成する
                for ($i = 0 ; $i < 10; $i++) {
                    $image_file = $faker->image($this->image_path, 320, 240, null, false);
                    $post_date = $faker->dateTime();
                    DB::table('posts')->insert([
                        'image_filepath' => $image_file,
                        'content' => $faker->text(200),
                        'user_id' => $user->id,
                        'created_at' => $post_date,
                        'updated_at' => $post_date,
                    ]);
                }
            }

        }
    }
}
