<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $user_names = [
            'alice',
            'bob',
            'carol',
            'dave',
            'eve',
            'sample',
        ];

        foreach ($user_names as $name) {
            DB::table('users')->insert([
               'name' => $name,
                'email' => "{$name}@gmail.com",
                'password' => bcrypt('sample'),
                'public_flag' => rand(0,1),
            ]);
        }
    }
}
