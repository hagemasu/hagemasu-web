[![pipeline status](https://gitlab.com/hagemasu/hagemasu-web/badges/master/pipeline.svg)](https://gitlab.com/hagemasu/hagemasu-web/commits/master)

## ハゲますWeb

ハゲますアプリケーション - ハゲログ

### 機能

* 日々の頭髪の具合を撮影＆投稿する
* 過去の頭髪の具合が一覧で見れる

### セットアップ

[Wikiページ](https://gitlab.com/hagemasu/hagemasu-web/wikis/home)を参照