<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// model
use App\Post;
class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function allposts()
    {
        $posts = Post::whereHas('User', function($q) {
            $q->where('public_flag', 1);
        })->latest()->paginate(8);

        return view('allPosts', ['posts' => $posts]);
    }
    
}
