<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;

class UsersController extends Controller
{
    public function edit() {
        $user = Auth::user();
        return view('users.edit', ['user' => $user]);
    }
    
    public function update(User $user, Request $request) {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'public_flag' => 'required',
            ];
            
        if ($user->id === Auth::user()->id) {
            $request->validate($rules);

            $user->update([
                'name' => $request->name,
                'email' => $request->email,
                'public_flag' => $request->public_flag,
                ]);
        };

        return redirect('/');
    }
}
