<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller
{
   /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }
    
    public function index()
    {
        $posts = auth()->user()->posts()->latest()->paginate(6);
        return view('posts.index')->with('posts', $posts);
    }
    
    public function store(Request $request)
    {
        //バリデーションを付与　入力必須,　拡張子の限定、最大サイズ
        $varidatedData=$request -> validate([
             'file' => 'required|image|max:3000',
             ]);
      
       
        // 画像を保存
         $path = $request->file('file')
             ->store('images', ['disk' => 's3', 'visibility' => 'public']);

        // コメントは必須でなくても登録可とする
        $content = $request->input('content') ?? '';

        // 画像とcontent(内容)を保存する
        \App\Post::create([
            'content' => $content,
            'image_filepath' => Storage::disk('s3')->url($path),
            'user_id' => auth()->user()->id,
            ]
        );
        
        return redirect()->route('posts.index');
    }
}

